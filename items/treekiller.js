var world;

function init(event) {
    event.item.setCustomName("TreeKiller");
}

function attack(event) {
    world = event.player.getWorld();

    var targetMaterial;
    var blocksToRemove = [];
    var alreadyChecked = [];

    event.player.message("TreeKiller was used!");

    // only act if the clicked thing is a block
    if (event.type == 2) {
        var clicked = event.target;
        targetMaterial = clicked.getName() || "";;
        
        // only act if the clicked block is a log
        if ( targetMaterial.startsWith("minecraft:log") == true ) {
            event.setCanceled(1);

            // add the clicked block.pos to blocksToRemove
            var pos = {
                "x" : clicked.getX(),
                "y" : clicked.getY(),
                "z" : clicked.getZ()
            };

            blocksToRemove.push(pos);
            alreadyChecked.push(JSON.stringify(pos));

            var limit = 0;
            while ( limit < 50 && blocksToRemove.length > 0 ) {
                limit++;

                var neighbors = get_neighbors(blocksToRemove[0]);
                remove(blocksToRemove[0]);
                blocksToRemove.shift();

                for ( var n in neighbors ) {
                    if ( test_material(neighbors[n], targetMaterial) ) {
                        blocksToRemove.push(neighbors[n]);
                    }
                }
            }
        }
    }
}

//////

function test_material(pos, material) {
    if (material == world.getBlock(pos.x, pos.y, pos.z).getName()) {
        return true
    } else {
        return false
    }
}

// returns a list of the 26 neighboring positions
// does not include the pos itself
function get_neighbors(pos) {
    var neighbors = [];

    for ( var x=-1; x<2; x++ ) {
        for ( var y=-1; y<2; y++ ) {
            for ( var z=-1; z<2; z++ ) {
                if (!( x==0 && y==0 && z==0)) {
                    neighbors.push({
                                "x" : pos.x+x,
                                "y" : pos.y+y,
                                "z" : pos.z+z
                    });
                }
            }
        }
    }
    return neighbors;
}

function remove(pos) {
    world.getBlock(pos.x, pos.y, pos.z).remove();
}